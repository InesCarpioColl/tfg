#include "polygon.h"

///////////////////////////////////////

Polygon::Polygon()
{
    n_vertexes=0;
}

///////////////////////////////////////

Polygon::Polygon(vector<Point> vertexes, QColor color)
{
    polyVertexes=vertexes;
    n_vertexes=polyVertexes.size();
    polyColor=color;
}

///////////////////////////////////////

Polygon::Polygon(const Polygon &Plg)
{
    polyVertexes=Plg.polyVertexes;
    n_vertexes=polyVertexes.size();
    polyColor=Plg.polyColor;
}

///////////////////////////////////////

Polygon::~Polygon()
{

}

///////////////////////////////////////

Point Polygon::getPolyVertex(int index)
{
    return polyVertexes[index];
}

///////////////////////////////////////

vector<Point> Polygon::getPolyVertexes()
{
    return polyVertexes;
}

///////////////////////////////////////

int Polygon::getPolyNumVert()
{
    return n_vertexes;
}

///////////////////////////////////////

QColor Polygon::getPolyColor()
{
    return polyColor;
}

///////////////////////////////////////
