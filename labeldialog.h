#ifndef LABELDIALOG_H
#define LABELDIALOG_H

#include <QDialog>
#include <QColorDialog>
#include <QTextEdit>
#include <QPalette>
#include <QMessageBox>
#include <iostream>
#include "label.h"

class Label;

namespace Ui {
class LabelDialog;
}

class LabelDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LabelDialog(QWidget *parent = 0);
    ~LabelDialog();

    Label getMyLabel();
    QString getLabelName();
    QColor getLabelColor();
    int getLabelID();
    bool getLabelInstanciable();
    bool getLabelBoundingBox();

    void setLabelAttributes(Label current_label);
    void setMode(int selectedMode);

private Q_SLOTS:
    void on_saveLabelButton_clicked();
    void on_chooseColorButton_clicked();

private:
    Ui::LabelDialog *ui;
    QColor color;
    Label mynewlabel;
    QWidget *colorWidget;
    bool colorSelected;
    int mode; // 0 = add label    1 = edit label
};

#endif // LABELDIALOG_H
