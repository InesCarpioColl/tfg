#include "label.h"

///////////////////////////////////////

Label::Label()
{
    num_vertexes=0;
    num_polygones=0;
}

///////////////////////////////////////

Label::Label(QString name, int id, QColor color, bool instanciable, bool boundingbox)
{
    label_name=name;
    label_id=id;
    label_color=color;
    label_instanciable=instanciable;
    label_boundingbox=boundingbox;

    num_vertexes=0;
    num_polygones=0;
}

///////////////////////////////////////

Label::Label(const Label &Lbl)
{
    label_name=Lbl.label_name;
    label_id=Lbl.label_id;
    label_color=Lbl.label_color;
    label_instanciable=Lbl.label_instanciable;
    label_boundingbox=Lbl.label_boundingbox;

    num_vertexes=Lbl.num_vertexes;
    my_Vertexes=Lbl.my_Vertexes;

    num_polygones=Lbl.num_polygones;
    my_Polygones=Lbl.my_Polygones;
}

///////////////////////////////////////

Label::~Label()
{

}

///////////////////////////////////////

int Label::getLabelID()
{
    return label_id;
}

///////////////////////////////////////

QColor Label::getLabelColor()
{
    return label_color;
}

///////////////////////////////////////

QString Label::getLabelName()
{
    return label_name;
}

///////////////////////////////////////

bool Label::getLabelInstanciableState()
{
    return label_instanciable;
}

///////////////////////////////////////

bool Label::getLabelBoundingBoxState()
{
    return label_boundingbox;
}

///////////////////////////////////////

void Label::setLabelID(int new_ID)
{
    label_id=new_ID;
}

///////////////////////////////////////

void Label::setLabelColor(QColor new_color)
{
    label_color=new_color;
}

///////////////////////////////////////

void Label::setLabelName(QString new_name)
{
    label_name=new_name;
}

///////////////////////////////////////

void Label::setLabelInstanciableState(bool new_instanc_state)
{
    label_instanciable=new_instanc_state;
}

///////////////////////////////////////

void Label::setLabelBoundingBoxState(bool new_bbox_state)
{
    label_boundingbox=new_bbox_state;
}

///////////////////////////////////////

void Label::addVertex(Point newVertex)
{
    my_Vertexes.push_back(newVertex);
    num_vertexes = my_Vertexes.size();
}

///////////////////////////////////////

void Label::deleteLastVertex()
{
    my_Vertexes.erase(my_Vertexes.end());
    num_vertexes = my_Vertexes.size();
}

///////////////////////////////////////

Point Label::getVertex(int index)
{
    return my_Vertexes[index];
}

///////////////////////////////////////

vector <Point> Label::getVertexes()
{
    return my_Vertexes;
}

///////////////////////////////////////

int Label::getNumVertexes()
{
    return my_Vertexes.size();
}

///////////////////////////////////////

void Label::createPolygon()
{
    Polygon *newPoly = new Polygon (my_Vertexes, label_color);
    my_Polygones.push_back(*newPoly);
    num_polygones=my_Polygones.size();

    my_Vertexes.clear();
    num_vertexes=my_Vertexes.size();
}

///////////////////////////////////////

void Label::addPolygon(Polygon poly)
{
    my_Polygones.push_back(poly);
    num_polygones=my_Polygones.size();
}

///////////////////////////////////////

Polygon Label::getPolygon(int index)
{
    return my_Polygones[index];
}

///////////////////////////////////////

vector<Polygon> Label::getPolygones()
{
    return my_Polygones;
}

///////////////////////////////////////

int Label::getNumPolygones()
{
    return num_polygones;
}

///////////////////////////////////////

void Label::deleteLastPolygon()
{
    my_Polygones.erase(my_Polygones.end());
    num_polygones = my_Polygones.size();
}

///////////////////////////////////////
