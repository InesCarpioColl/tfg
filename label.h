#ifndef LABEL
#define LABEL

#include <QWidget>
#include <iostream>
#include <opencv2/opencv.hpp>
#include "polygon.h"

using namespace std;
using namespace cv;

class Polygon;

class Label{

    int label_id;
    QString label_name;
    QColor label_color;
    bool label_instanciable;
    bool label_boundingbox;

    vector<Point> my_Vertexes; // OpenCV point class
    int num_vertexes;
    vector<Polygon> my_Polygones;
    int num_polygones;


public:
    Label();
    Label(QString name, int id, QColor color, bool instanciable, bool boundingbox);
    Label(const Label &Lbl);
    ~Label();

    int getLabelID();
    QString getLabelName();
    QColor getLabelColor();
    bool getLabelInstanciableState();
    bool getLabelBoundingBoxState();

    void setLabelID(int new_ID);
    void setLabelName(QString new_name);
    void setLabelColor(QColor new_color);
    void setLabelInstanciableState(bool new_instanc_state);
    void setLabelBoundingBoxState(bool new_bbox_state);

    void addVertex(Point newVertex);
    void deleteLastVertex();
    Point getVertex(int index);
    vector<Point> getVertexes();
    int getNumVertexes();

    void createPolygon();
    void addPolygon(Polygon poly);
    Polygon getPolygon(int index);
    vector<Polygon> getPolygones();
    int getNumPolygones();
    void deleteLastPolygon();
};
#endif // LABEL
