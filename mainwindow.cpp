﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

///////////////////////////////////////

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Menu Options
    openImageAct = ui->openImageAct;
    openVideoAct = ui->openVideoAct;
    openLabelsAct = ui->openLabelsAct;
    exportDatasetAct = ui->exportDatasetAct;
    exportLabelsAct = ui->exportLabelsAct;

    // Displaying Image Label
    label_image = new QLabel;
    label_image->setBackgroundRole(QPalette::Base);
    label_image->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    label_image->setScaledContents(true);

    scrollArea = new QScrollArea;
    scrollArea->setWidget(label_image);

    // Initialize variables
    labelling_checked=false;
    labelSelected=false;
    index_label=-1;
    newPolygnsSaved=true;
    auxmenu='n';

    setWindowTitle(tr("Labelling Tool"));

    /*
     * CONNECT SLOTS AND FUNCTIONS
     */

    // Menu Options
    connect(openImageAct,SIGNAL(triggered()),this,SLOT(openImage()));
    connect(openVideoAct,SIGNAL(triggered()),this,SLOT(openVideo()));
    connect(openLabelsAct,SIGNAL(triggered()),this,SLOT(openLabels()));
    connect(exportDatasetAct,SIGNAL(triggered()),this,SLOT(exportDatasetAction()));
    connect(exportLabelsAct,SIGNAL(triggered()),this,SLOT(exportLabelsAction()));
}

///////////////////////////////////////

MainWindow::~MainWindow()
{
    delete ui;
}

///////////////////////////////////////

void MainWindow::openImage()
{
    if(newPolygnsSaved==false)
    {
        QMessageBox newDecision;
        newDecision.setWindowTitle("New image");
        newDecision.setText("Do you want to go to open a new image?");
        newDecision.setInformativeText("If you change without saving Polygones, you'll lose this information.");
        newDecision.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        newDecision.setDefaultButton(QMessageBox::No);
        newDecision.setIcon(QMessageBox::Question);
        int ret = newDecision.exec();

        if (ret == QMessageBox::No)
            return;

        else if (ret == QMessageBox::Yes)
            newPolygnsSaved=true;
    }

    // Clean former vertexes and polygones
    int numVertexes, numPolygones;

    for (int i=0; i<my_Labels.size(); i++)
    {
        // Delete all vertexes
        numVertexes=my_Labels[i].getNumVertexes();
        for (int j=0; j<numVertexes; j++)
            my_Labels[i].deleteLastVertex();

        // Delete all polygones
        numPolygones=my_Labels[i].getNumPolygones();
        for (int k=0; k<numPolygones; k++)
            my_Labels[i].deleteLastPolygon();
    }

    auxmenu='i';

    // Load all files names
    imageFilesNames = QFileDialog::getOpenFileNames(this, tr("Open Image"), QDir::currentPath(),tr("Images (*.png *.xpm *.jpg)"));

    num_image = imageFilesNames.size();
    if (num_image==0)
        return;

    // Load first image
    image_index=0;
    fileName=imageFilesNames.at(image_index);

    if (!fileName.isEmpty())
    {
        ui->frameLabel->setText("Image");

        QImage image(fileName);

        if (image.isNull())
        {
            QMessageBox::information(this, tr("Image Viewer"), tr("Cannot load %1.").arg(fileName));
            return;
        }

        // Check dimensions
        height_label = ui->labelImage->height();
        width_label = ui->labelImage->width();
        height_image = image.height();
        width_image = image.width();

        bool scaleImage = scaledImage();
        if(scaleImage) // Scale to label dimensions
        {
            image=image.scaled(ui->labelImage->size(), Qt::KeepAspectRatio);
            height_image = image.height();
            width_image = image.width();
        }

        // Set labelling limits
        setLimits();

        // Get Mat image
        QImage   swapped = image;
        if ( image.format() == QImage::Format_RGB32 )
            swapped = swapped.convertToFormat(QImage::Format_RGB888);

        swapped = swapped.rgbSwapped();

        cvImg = Mat(swapped.height(), swapped.width(),CV_8UC3,const_cast<uchar*>(swapped.bits()),
                    static_cast<size_t>(swapped.bytesPerLine())).clone();

        // Show image
        ui->labelImage->setPixmap(QPixmap::fromImage(image));
        ui->labelImage->setAlignment(Qt::AlignCenter | Qt::AlignCenter);

        label_image->show();

        // Set current image and total image labels
        QString numimage_string = QString::number(num_image,10);
        ui->numFramesLabel->setText(numimage_string);
        update_currentFrame();

        // Search for polygones file
        bool polygonesLoad = searchPolygonesFile();

        if (polygonesLoad==true)
            paint();
    }
    else
        return;
}

///////////////////////////////////////

void MainWindow::openVideo()
{
    if(newPolygnsSaved==false)
    {
        QMessageBox newDecision;
        newDecision.setWindowTitle("New video");
        newDecision.setText("Do you want to go to open a new video?");
        newDecision.setInformativeText("If you change without saving Polygones, you'll lose this information.");
        newDecision.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        newDecision.setDefaultButton(QMessageBox::No);
        newDecision.setIcon(QMessageBox::Question);
        int ret = newDecision.exec();

        if (ret == QMessageBox::No)
            return;

        else if (ret == QMessageBox::Yes)
            newPolygnsSaved=true;
    }

    // Clean former vertexes and polygones
    int numVertexes, numPolygones;

    for (int i=0; i<my_Labels.size(); i++)
    {
        // Delete all vertexes
        numVertexes=my_Labels[i].getNumVertexes();
        for (int j=0; j<numVertexes; j++)
            my_Labels[i].deleteLastVertex();

        // Delete all polygones
        numPolygones=my_Labels[i].getNumPolygones();
        for (int k=0; k<numPolygones; k++)
            my_Labels[i].deleteLastPolygon();
    }

    auxmenu='v';

    fileName = QFileDialog::getOpenFileName(this, tr("Open Video"), QDir::currentPath(), tr("Videos (*.avi *.mpg)"));

    if (!fileName.isEmpty())
    {
        ui->frameLabel->setText("Frame");

        //name of the video
        string name= fileName.toStdString();

        //Load video
        cap.open(name);

        if(!cap.isOpened())
        {
            cout<< "error in VideoCatpure: check path file" << endl;
            return;
        }

        // https://stackoverflow.com/questions/11260042/reverse-video-playback-in-opencv?answertab=oldest#tab-top
        double frame_rate = cap.get(CV_CAP_PROP_FPS);

        // Calculate number of msec per frame.
        // (msec/sec / frames/sec = msec/frame)
        frame_msec = 1000 / frame_rate;

        num_frame = cap.get(CV_CAP_PROP_FRAME_COUNT);
        current_frame = 1;

        QString numframes_string = QString::number(num_frame,10);
        ui->numFramesLabel->setText(numframes_string);

        //check the success for image reading
        bool success;

        //read next frame
        success = cap.read(frame);

        if (success == false)
        {
            cout << "Cannot read the frame from file" << endl;
            return;
        }

        QImage videoFrame(frame.data, frame.cols, frame.rows, frame.step, QImage::Format_RGB888);
        QImage videoFrameRGB=videoFrame.rgbSwapped();

        // Check dimensions
        height_label = ui->labelImage->height();
        width_label = ui->labelImage->width();
        height_image = videoFrameRGB.height();
        width_image = videoFrameRGB.width();

        bool scaleImage = scaledImage();
        if(scaleImage) // Scale to label dimensions
        {
            videoFrameRGB = videoFrameRGB.scaled(ui->labelImage->size(), Qt::KeepAspectRatio);
            height_image = videoFrameRGB.height();
            width_image = videoFrameRGB.width();
            Size resizeMat(width_image, height_image);
            cv::resize(frame, frame, resizeMat);
        }

        // Set labelling limits
        setLimits();

        ui->labelImage->setPixmap(QPixmap::fromImage(videoFrameRGB));
        ui->labelImage->setAlignment(Qt::AlignCenter | Qt::AlignCenter);

        label_image->show();

        if (waitKey(frame_msec) >= 0)
            return;

        update_currentFrame();

        // Search for polygones file
        bool polygonesLoad = searchPolygonesFile();

        if (polygonesLoad==true)
            paint();
    }
    else
        return;
}

///////////////////////////////////////

bool MainWindow::scaledImage()
{
    int diffHeight = height_image - height_label;
    int diffWidth = width_image - width_label;

    if (diffHeight<=0 && diffWidth<=0)
        return false;
    else // Need to be scaled
        return true;
}

///////////////////////////////////////

bool MainWindow::searchPolygonesFile()
{
    // File Name
    string fileNamePolyString = fileName.toStdString();
    int sizeFileName = fileNamePolyString.length();
    fileNamePolyString.erase(sizeFileName-4); // Dirección sin el .jpg, .png, .avi, .mpg

    if (auxmenu == 'v')
    {
        fileNamePolyString = fileNamePolyString + '_';

        // Add current frame to the name of the file
        stringstream stringCurrentFrame;
        stringCurrentFrame << fileNamePolyString << current_frame;

        fileNamePolyString = stringCurrentFrame.str();
    }

    fileNamePolyString = fileNamePolyString + "_Polygones.xml";

    const char *fileNamePolyChar = fileNamePolyString.c_str();

    // Load file
    XMLDocument xml_openPolygones;
    XMLError result = xml_openPolygones.LoadFile(fileNamePolyChar);
    if (result != tinyxml2::XML_SUCCESS)
        return false;

    // File found
    QMessageBox newDecision;
    newDecision.setWindowTitle("Polygones file");
    newDecision.setText("There is a file related to this image that contains labeled polygons.");
    newDecision.setInformativeText("Do you want to load it?");
    newDecision.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    newDecision.setDefaultButton(QMessageBox::Yes);
    newDecision.setIcon(QMessageBox::Question);
    int ret = newDecision.exec();

    if (ret == QMessageBox::No)
        return false;

    else if (ret == QMessageBox::Yes)
    {
        // If there're current Labels
        if (my_Labels.size()>0)
        {
            //Initialize Selected Label Area and index_label
            labelSelected=false;
            index_label=-1;

            ui -> line_LabelName -> clear();
            ui -> line_LabelID -> clear();
            ui -> LabelColor_Widget -> setAutoFillBackground(false);
            ui -> lineInstanciable -> clear();
            ui -> lineBBox -> clear();

            // Make a Decision
            QMessageBox new_Decision;
            new_Decision.setWindowTitle("New Labels");
            new_Decision.setText("Do you want to replace current Labels with new related-to-polygons Labels?");
            new_Decision.setInformativeText("If you click 'No', the new tags will be added below the current ones");
            new_Decision.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            new_Decision.setDefaultButton(QMessageBox::Yes);
            new_Decision.setIcon(QMessageBox::Question);
            int ret = new_Decision.exec();

            if (ret == QMessageBox::Yes)
            {
                my_Labels.clear();
                ui->listWidget->clear();
            }
            else if (ret == QMessageBox::No)
            {
                // Delete polygones of previous images
                int numPolygones;

                for (int i=0; i<my_Labels.size(); i++)
                {
                    // Delete all polygones from other images
                    numPolygones=my_Labels[i].getNumPolygones();
                    for (int k=0; k<numPolygones; k++)
                        my_Labels[i].deleteLastPolygon();
                }
            }
        }

        XMLNode *pRoot = xml_openPolygones.FirstChild();

        // Variables
        Label *loadLabel;
        QString *nameLabel;
        string nameLabelString;
        int numLabels, IDLabel, redLabel, greenLabel, blueLabel, numPolygns, numVertexes, Xcoord, Ycoord;
        QColor colorLabel;
        bool instLabel, bboxLabel;
        Polygon *loadPolygon;
        vector<Point> loadVertexes;
        Point *loadVertex;

        XMLElement *pLabelElement, *pNameElement, *pIDElement, *pColorElement, *pColorLevel, *pInstElement, *pBBoxElement,
                *pNumPolygnsELement, *pPolygonElement, *pNumVertexElement, *pVertexElement, *pXVertex, *pYVertex;

        // Num of Labels
        XMLElement *pNumLabelElement = pRoot->FirstChildElement("NumLabels");
        pNumLabelElement->QueryIntText(&numLabels);

        // Get Labels attributes
        for(pLabelElement= pRoot->FirstChildElement("Label"); pLabelElement !=NULL;
            pLabelElement=pLabelElement->NextSiblingElement("Label"))
        {
            // Name
            pNameElement = pLabelElement->FirstChildElement("Name");
            nameLabelString = pNameElement->GetText();
            nameLabel = new QString(nameLabelString.c_str());

            // ID
            pIDElement = pLabelElement->FirstChildElement("ID");
            pIDElement->QueryIntText(&IDLabel);

            // Color
            pColorElement = pLabelElement->FirstChildElement("Color");

            pColorLevel = pColorElement->FirstChildElement("Red");
            pColorLevel->QueryIntText(&redLabel);
            colorLabel.setRed(redLabel);

            pColorLevel = pColorElement->FirstChildElement("Green");
            pColorLevel->QueryIntText(&greenLabel);
            colorLabel.setGreen(greenLabel);

            pColorLevel = pColorElement->FirstChildElement("Blue");
            pColorLevel->QueryIntText(&blueLabel);
            colorLabel.setBlue(blueLabel);

            // Instanciable
            pInstElement = pLabelElement->FirstChildElement("Instanciable");
            pInstElement->QueryBoolText(&instLabel);

            // Bounding Box
            pBBoxElement = pLabelElement->FirstChildElement("BoundingBox");
            pBBoxElement->QueryBoolText(&bboxLabel);

            // Create label
            loadLabel = new Label(*nameLabel, IDLabel, colorLabel, instLabel, bboxLabel);

            // Add to Labels vector and List Widget
            my_Labels.push_back(*loadLabel);
            ui->listWidget->addItem(*nameLabel);

            // Num of Polygones
            pNumPolygnsELement = pLabelElement->FirstChildElement("NumPolygones");
            pNumPolygnsELement->QueryIntText(&numPolygns);

            // Get Polygones attributes
            for(pPolygonElement = pLabelElement->FirstChildElement("Polygon"); pPolygonElement !=NULL;
                pPolygonElement = pPolygonElement->NextSiblingElement("Polygon"))
            {
                // Num of Vertexes
                pNumVertexElement = pPolygonElement->FirstChildElement("NumVertexes");
                pNumVertexElement->QueryIntText(&numVertexes);

                // Get All Vertexes
                for(pVertexElement = pPolygonElement->FirstChildElement("Vertex"); pVertexElement !=NULL;
                    pVertexElement = pVertexElement->NextSiblingElement("Vertex"))
                {
                    // Get X coordinate
                    pXVertex = pVertexElement->FirstChildElement("X");
                    pXVertex->QueryIntText(&Xcoord);

                    // Get Y coordinate
                    pYVertex = pVertexElement->FirstChildElement("Y");
                    pYVertex->QueryIntText(&Ycoord);

                    // Create vertex
                    loadVertex = new Point(Xcoord, Ycoord);

                    // Add to load Vertexes vector
                    loadVertexes.push_back(*loadVertex);
                }

                // Create polygon
                loadPolygon = new Polygon(loadVertexes, colorLabel);

                // Add to Polygones vector in Label
                my_Labels.back().addPolygon(*loadPolygon);

                // Clear Vertex vector
                loadVertexes.clear();
            }
        }

        return true;
    }
}

///////////////////////////////////////

void MainWindow::openLabels()
{
    QString fileLabelsName = QFileDialog::getOpenFileName(this, tr("Open Labels File"), QDir::currentPath(), tr("XML files (*.xml)"));

    if (!fileLabelsName.isEmpty())
    {
        string fileLabelsNameString = fileLabelsName.toStdString();
        const char *fileLabelsNameChar = fileLabelsNameString.c_str();

        // Load file
        XMLDocument xml_openLabels;
        xml_openLabels.LoadFile(fileLabelsNameChar);

        XMLNode *pRoot = xml_openLabels.FirstChild();

        // Num of Labels
        int numLabels;
        XMLElement *pNumLabelElement = pRoot->FirstChildElement("NumLabels");
        pNumLabelElement->QueryIntText(&numLabels);

        // Variables
        Label *loadLabel;
        QString *nameLabel;
        string nameLabelString;
        int IDLabel, redLabel, greenLabel, blueLabel;
        QColor colorLabel;
        bool instLabel, bboxLabel;

        XMLElement *pNameElement, *pIDElement, *pColorElement, *pColorLevel, *pInstElement, *pBBoxElement;

        // Get Labels attributes
        for(XMLElement *pLabelElement= pRoot->FirstChildElement("Label"); pLabelElement !=NULL;
            pLabelElement=pLabelElement->NextSiblingElement("Label"))
        {
            // Name
            pNameElement = pLabelElement->FirstChildElement("Name");
            nameLabelString = pNameElement->GetText();
            nameLabel = new QString(nameLabelString.c_str());

            // ID
            pIDElement = pLabelElement->FirstChildElement("ID");
            pIDElement->QueryIntText(&IDLabel);

            // Color
            pColorElement = pLabelElement->FirstChildElement("Color");

            pColorLevel = pColorElement->FirstChildElement("Red");
            pColorLevel->QueryIntText(&redLabel);
            colorLabel.setRed(redLabel);

            pColorLevel = pColorElement->FirstChildElement("Green");
            pColorLevel->QueryIntText(&greenLabel);
            colorLabel.setGreen(greenLabel);

            pColorLevel = pColorElement->FirstChildElement("Blue");
            pColorLevel->QueryIntText(&blueLabel);
            colorLabel.setBlue(blueLabel);

            // Instanciable
            pInstElement = pLabelElement->FirstChildElement("Instanciable");
            pInstElement->QueryBoolText(&instLabel);

            // Bounding Box
            pBBoxElement = pLabelElement->FirstChildElement("BoundingBox");
            pBBoxElement->QueryBoolText(&bboxLabel);

            // Create label
            loadLabel = new Label(*nameLabel, IDLabel, colorLabel, instLabel, bboxLabel);

            // Add to Labels vector and List Widget
            my_Labels.push_back(*loadLabel);
            ui->listWidget->addItem(*nameLabel);
        }
    }
}

///////////////////////////////////////

void MainWindow::setLimits()
{
    // IMAGE LABEL POSITION: x 13  y 27

    QPoint C;
    C.setX(13+width_label/2);
    C.setY(27+height_label/2);

    P1.setX(C.x()-width_image/2);
    P1.setY(C.y()-height_image/2);

    P2.setX(C.x()+width_image/2);
    P2.setY(C.y()+height_image/2);
}

///////////////////////////////////////

void MainWindow::on_prevButton_clicked()
{
    if (auxmenu=='i' && num_image<=1)
        return;

    // Check frame interval
    getInterval();

    if(newPolygnsSaved==false)
    {
        QMessageBox newDecision;
        newDecision.setWindowTitle("Previous image");
        newDecision.setText("Do you want to go to previous image?");
        newDecision.setInformativeText("If you change without saving Polygones, you'll lose this information.");
        newDecision.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        newDecision.setDefaultButton(QMessageBox::No);
        newDecision.setIcon(QMessageBox::Question);
        int ret = newDecision.exec();

        if (ret == QMessageBox::No)
            return;
        else if (ret == QMessageBox::Yes)
            newPolygnsSaved=true;
    }

    int numVertexes, numPolygones;

    for (int i=0; i<my_Labels.size(); i++)
    {
        // Delete all vertexes
        numVertexes=my_Labels[i].getNumVertexes();
        for (int j=0; j<numVertexes; j++)
            my_Labels[i].deleteLastVertex();

        // Delete all polygones
        numPolygones=my_Labels[i].getNumPolygones();
        for (int k=0; k<numPolygones; k++)
            my_Labels[i].deleteLastPolygon();
    }

    if (auxmenu=='v') // Video
        prevVideoFrame();
    else if (auxmenu=='i') // Image
        prevImage();
    else
        return;
}

///////////////////////////////////////

void MainWindow::prevImage()
{
    // Get new position considering the value of the interval
    image_index = image_index - interval;

    if (image_index < 0)
    {
        int overflow = num_image + image_index;
        image_index = overflow;
    }

    //Load prev file name
    fileName=imageFilesNames.at(image_index);

    if (!fileName.isEmpty())
    {
        QImage image(fileName);

        if (image.isNull())
        {
            QMessageBox::information(this, tr("Image Viewer"), tr("Cannot load %1.").arg(fileName));
            return;
        }

        // Check dimensions
        height_label = ui->labelImage->height();
        width_label = ui->labelImage->width();
        height_image = image.height();
        width_image = image.width();

        bool scaleImage = scaledImage();
        if(scaleImage) // Scale to label dimensions
        {
            image=image.scaled(ui->labelImage->size(), Qt::KeepAspectRatio);
            height_image = image.height();
            width_image = image.width();
        }

        // Set labelling limits
        setLimits();

        // Get Mat image
        QImage   swapped = image;
        if ( image.format() == QImage::Format_RGB32 )
            swapped = swapped.convertToFormat(QImage::Format_RGB888);

        swapped = swapped.rgbSwapped();

        cvImg = Mat(swapped.height(), swapped.width(),CV_8UC3,const_cast<uchar*>(swapped.bits()),
                    static_cast<size_t>(swapped.bytesPerLine())).clone();

        // Show image
        ui->labelImage->setPixmap(QPixmap::fromImage(image));
        ui->labelImage->setAlignment(Qt::AlignCenter | Qt::AlignCenter);

        label_image->show();

        // Set current image and total image labels
        QString numimage_string = QString::number(num_image,10);
        ui->numFramesLabel->setText(numimage_string);
        update_currentFrame();

        // Search for polygones file
        bool polygonesLoad = searchPolygonesFile();

        if (polygonesLoad==true)
            paint();
    }
}

///////////////////////////////////////

void MainWindow::prevVideoFrame()
{
    // Get the current position of the video in msecond
    double video_time = cap.get(CV_CAP_PROP_POS_MSEC);

    // Decrease video time by number of msec in as many frames as the interval indicates and seek to the new time.
    // It's necessary to go to one more frame, because the read() function always reads the next frame,
    // which is the one we want to reach
    video_time = video_time - (interval+1)*frame_msec;
    cap.set(CV_CAP_PROP_POS_MSEC, video_time);

    bool success = cap.read(frame);

    if (success == false)
    {
        cout << "Cannot read the frame from file" << endl;
        return;
    }

    QImage videoFrame(frame.data, frame.cols, frame.rows, frame.step, QImage::Format_RGB888);
    QImage videoFrameRGB=videoFrame.rgbSwapped();

    // Check dimensions
    height_image = videoFrameRGB.height();
    width_image = videoFrameRGB.width();

    bool scaleImage = scaledImage();
    if(scaleImage) // Scale to label dimensions
    {
        videoFrameRGB = videoFrameRGB.scaled(ui->labelImage->size(), Qt::KeepAspectRatio);
        height_image = videoFrameRGB.height();
        width_image = videoFrameRGB.width();
        Size resizeMat(width_image, height_image);
        cv::resize(frame, frame, resizeMat);
    }

    // Set labelling limits
    setLimits();

    // Show frame
    ui->labelImage->setPixmap(QPixmap::fromImage(videoFrameRGB));
    ui->labelImage->setAlignment(Qt::AlignCenter | Qt::AlignCenter);

    label_image->show();

    current_frame = current_frame-interval;
    update_currentFrame();

    if (waitKey(frame_msec) >= 0)
        return;

    // Search for polygones file
    bool polygonesLoad = searchPolygonesFile();

    if (polygonesLoad==true)
        paint();
}

///////////////////////////////////////

void MainWindow::on_nextButton_clicked()
{
    if (auxmenu=='i' && num_image<=1)
        return;

    // Check frame interval
    getInterval();

    if(newPolygnsSaved==false)
    {
        QMessageBox newDecision;
        newDecision.setWindowTitle("Next image");
        newDecision.setText("Do you want to go to next image?");
        newDecision.setInformativeText("If you change without saving Polygones, you'll lose this information.");
        newDecision.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        newDecision.setDefaultButton(QMessageBox::No);
        newDecision.setIcon(QMessageBox::Question);
        int ret = newDecision.exec();

        if (ret == QMessageBox::No)
            return;
        else if (ret == QMessageBox::Yes)
            newPolygnsSaved=true;
    }

    int numVertexes, numPolygones;

    for (int i=0; i<my_Labels.size(); i++)
    {
        // Delete all vertexes
        numVertexes=my_Labels[i].getNumVertexes();
        for (int j=0; j<numVertexes; j++)
            my_Labels[i].deleteLastVertex();

        // Delete all polygones
        numPolygones=my_Labels[i].getNumPolygones();
        for (int k=0; k<numPolygones; k++)
            my_Labels[i].deleteLastPolygon();
    }

    if (auxmenu=='v') // Video
        nextVideoFrame();
    else if (auxmenu=='i') // Image
        nextImage();
    else
        return;
}

///////////////////////////////////////

void MainWindow::nextImage()
{
    // Get new position considering the value of the interval
    image_index = image_index + interval;

    if (image_index > (num_image-1))
    {
        int overflow = image_index - num_image;
        image_index = overflow;
    }

    //Load next file name
    fileName=imageFilesNames.at(image_index);

    if (!fileName.isEmpty())
    {
        QImage image(fileName);

        if (image.isNull())
        {
            QMessageBox::information(this, tr("Image Viewer"), tr("Cannot load %1.").arg(fileName));
            return;
        }

        // Check dimensions
        height_label = ui->labelImage->height();
        width_label = ui->labelImage->width();
        height_image = image.height();
        width_image = image.width();

        bool scaleImage = scaledImage();
        if(scaleImage) // Scale to label dimensions
        {
            image=image.scaled(ui->labelImage->size(), Qt::KeepAspectRatio);
            height_image = image.height();
            width_image = image.width();
        }

        // Set labelling limits
        setLimits();

        // Get Mat image
        QImage   swapped = image;
        if ( image.format() == QImage::Format_RGB32 )
            swapped = swapped.convertToFormat(QImage::Format_RGB888);

        swapped = swapped.rgbSwapped();

        cvImg = Mat(swapped.height(), swapped.width(),CV_8UC3,const_cast<uchar*>(swapped.bits()),
                    static_cast<size_t>(swapped.bytesPerLine())).clone();

        // Show image
        ui->labelImage->setPixmap(QPixmap::fromImage(image));
        ui->labelImage->setAlignment(Qt::AlignCenter | Qt::AlignCenter);

        label_image->show();

        // Set current image label
        QString numimage_string = QString::number(num_image,10);
        ui->numFramesLabel->setText(numimage_string);
        update_currentFrame();

        // Search for polygones file
        bool polygonesLoad = searchPolygonesFile();

        if (polygonesLoad==true)
            paint();
    }
}

///////////////////////////////////////

void MainWindow::nextVideoFrame()
{
    for (int i=0; i<interval; i++)
    {
        //check the success for image reading
        bool success;

        //read next frame
        success = cap.read(frame);

        if (success == false)
        {
            cout << "Cannot read the frame from file" << endl;
            return;
        }

        current_frame++;
    }

    // Set QImage from cv::Mat
    QImage videoFrame(frame.data, frame.cols, frame.rows, frame.step, QImage::Format_RGB888);
    QImage videoFrameRGB=videoFrame.rgbSwapped();

    // Check dimensions
    height_image = videoFrameRGB.height();
    width_image = videoFrameRGB.width();

    bool scaleImage = scaledImage();
    if(scaleImage) // Scale to label dimensions
    {
        videoFrameRGB = videoFrameRGB.scaled(ui->labelImage->size(), Qt::KeepAspectRatio);
        height_image = videoFrameRGB.height();
        width_image = videoFrameRGB.width();
        Size resizeMat(width_image, height_image);
        cv::resize(frame, frame, resizeMat);
    }

    // Set labelling limits
    setLimits();

    // Show frame
    ui->labelImage->setPixmap(QPixmap::fromImage(videoFrameRGB));
    ui->labelImage->setAlignment(Qt::AlignCenter | Qt::AlignCenter);

    label_image->show();

    if (waitKey(frame_msec) >= 0)
        return;

    update_currentFrame();

    // Search for polygones file
    bool polygonesLoad = searchPolygonesFile();

    if (polygonesLoad==true)
        paint();
}

///////////////////////////////////////

void MainWindow::getInterval()
{
    //transform num of interval frames from QString to int
    QString textInterval = ui -> lineInterval -> text();
    bool ok;
    interval=textInterval.toInt(&ok,10);
}

///////////////////////////////////////

void MainWindow::update_currentFrame()
{
    QString current_string;
    if (auxmenu=='v') // Video
    {
        if(current_frame<0)
            current_frame=1;
        else if(current_frame>num_frame)
            current_frame=num_frame;

        current_string = QString::number(current_frame,10);
    }
    else if (auxmenu=='i') // Image
        current_string = QString::number(image_index+1,10);

    ui->currentFrameLabel->setText(current_string);
}

///////////////////////////////////////

void MainWindow::on_labellingButton_toggled(bool checked)
{
    // Checked = true si está activado, false si está desactivado
    labelling_checked = checked;
}

///////////////////////////////////////

void MainWindow::mouseDoubleClickEvent(QMouseEvent *doubleclick)
{
    if (labelling_checked==false)
        return;

    if (labelSelected==false)
    {
        QMessageBox::warning(this, tr("Labelling error"), tr("Please, select a valid Label"));
        return;
    }

    // Create new Qt Vertex
    QPoint *new_vertex = new QPoint;
    *new_vertex = doubleclick->pos();

    if((new_vertex->x() < P1.x()) || (new_vertex->x() > P2.x()))
    {
        QMessageBox::warning(this, tr("Labelling error"), tr("Please, set new vertex within the image limits"));
        return;
    }
    else if((new_vertex->y() < P1.y()) || (new_vertex->y() > P2.y()))
    {
        QMessageBox::warning(this, tr("Labelling error"), tr("Please, set new vertex within the image limits"));
        return;
    }
    else{
        // Create OpenCV Vertex

        // Calculate vertex position regarding the reference system of the image (P1 point)
        int Xcv = new_vertex->x() - P1.x();
        int Ycv = new_vertex->y() - P1.y();

        Point *cv_vertex = new Point (Xcv, Ycv);

        int numVertexes = my_Labels[index_label].getNumVertexes();
        bool vertexes_match=false;

        if (numVertexes>0) // Ya se ha creado el primer vértice
        {
            Point aux_vertex = my_Labels[index_label].getVertex(0);

            // Check if the new Vertex match with the first Vertex
            if ((Xcv>=aux_vertex.x-10 && Xcv<=aux_vertex.x+10) && (Ycv>=aux_vertex.y-10 && Ycv<=aux_vertex.y+10))
                vertexes_match=true;
        }

        newPolygnsSaved = false;

        if(vertexes_match==false || vertexes_match==true && my_Labels[index_label].getNumVertexes()<3)
        {
            // Add Vertex to vector
            my_Labels[index_label].addVertex(*cv_vertex);

            // Draw Vertex
            paint();
        }
        else
        {
            QMessageBox newDecision;
            newDecision.setWindowTitle("Create new polygon");
            newDecision.setText("Do you want to create a polygon?");
            newDecision.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            newDecision.setDefaultButton(QMessageBox::Yes);
            newDecision.setIcon(QMessageBox::Question);
            int ret = newDecision.exec();

            if (ret == QMessageBox::Yes)
            {
                // Create and add Polygon to the label
                my_Labels[index_label].createPolygon();

                // Draw new Polygon
                paint();
            }
            else if (ret == QMessageBox::No)
            {
                // Add Vertex to vector
                my_Labels[index_label].addVertex(*cv_vertex);

                // Draw new Vertex
                paint();
            }
        }
    }
}

///////////////////////////////////////

void MainWindow::paint()
{
    Mat imgDrawn;

    if (auxmenu=='i')
        imgDrawn = cvImg.clone();
    else if (auxmenu=='v')
        imgDrawn = frame.clone();

    // Draw all existing polygones
    QColor label_color;
    int num_polygones_label;
    Polygon label_polygon;
    int num_vertexes_polygon;
    Rect BoundingBox;

    for (int i=0; i<my_Labels.size(); i++)
    {
        num_polygones_label=my_Labels[i].getNumPolygones();
        label_color=my_Labels[i].getLabelColor();

        for (int j=0; j<num_polygones_label; j++)
        {
            label_polygon = my_Labels[i].getPolygon(j);

            num_vertexes_polygon = label_polygon.getPolyNumVert();
            int *pNumVertex = &num_vertexes_polygon;

            Point poly_Vertexes[num_vertexes_polygon];
            for (int k=0; k<num_vertexes_polygon; k++)
                poly_Vertexes[k] = label_polygon.getPolyVertex(k);
            const Point *pVertex = poly_Vertexes;
            const Point **pPointer = &pVertex;

            fillPoly(imgDrawn, pPointer, pNumVertex, 1,
                           Scalar(label_color.blue(), label_color.green(), label_color.red()));

            if(my_Labels[i].getLabelBoundingBoxState() == true)
            {
                BoundingBox = boundingRect(label_polygon.getPolyVertexes());
                rectangle(imgDrawn, BoundingBox, Scalar(0,0,0), 2);
            }
        }
    }

    if(index_label>=0)
    {
        // Draw a circle to mark Vertexes and a line to connect them
        QColor color = my_Labels[index_label].getLabelColor();
        int numVertexes = my_Labels[index_label].getNumVertexes();

        for (int i=0; i<numVertexes; i++)
        {
            circle(imgDrawn, my_Labels[index_label].getVertex(i), 3, Scalar(0, 0, 0)); // Black edge
            circle(imgDrawn, my_Labels[index_label].getVertex(i), 2, Scalar(color.blue(), color.green(), color.red()), -1 ); // BGR format

            // Draw Lines if there are more than one vertex
            if(i>0)
            {
                line(imgDrawn, my_Labels[index_label].getVertex(i-1), my_Labels[index_label].getVertex(i), Scalar(0, 0, 0), 4);
                line(imgDrawn, my_Labels[index_label].getVertex(i-1), my_Labels[index_label].getVertex(i),
                     Scalar(color.blue(), color.green(), color.red()), 2);
            }
        }
    }

    // Show image in QLabel
    QImage vertexImage=QImage(imgDrawn.data, imgDrawn.cols, imgDrawn.rows, imgDrawn.step, QImage::Format_RGB888);
    QImage vertexImageRGB=vertexImage.rgbSwapped();

    ui->labelImage->setPixmap(QPixmap::fromImage(vertexImageRGB));
    ui->labelImage->setAlignment(Qt::AlignCenter | Qt::AlignCenter);

    label_image->show();

    imgDrawn.release();
}

///////////////////////////////////////

void MainWindow::on_deleteVertexButton_clicked()
{
    int numVertexes = my_Labels[index_label].getNumVertexes();

    if (numVertexes == 0)
    {
        QMessageBox::warning(this, tr("Deleting error"), tr("There aren't more vertexes"));
        return;
    }

    my_Labels[index_label].deleteLastVertex();
    paint();
}

///////////////////////////////////////

void MainWindow::on_deletePolyButton_clicked()
{
    int numPolygones = my_Labels[index_label].getNumPolygones();

    if (numPolygones == 0)
    {
        QMessageBox::warning(this, tr("Deleting error"), tr("There aren't more polygones"));
        return;
    }

    my_Labels[index_label].deleteLastPolygon();
    paint();
}

///////////////////////////////////////

void MainWindow::on_addButton_clicked()
{
    LabelDialog addLabel;
    addLabel.setWindowTitle("Add New Label");
    addLabel.setModal(true);
    addLabel.setMode(0);
    addLabel.exec();

    // Get new Label attributes
    Label *newLabel = new Label;
    *newLabel=addLabel.getMyLabel();

    // Avoid error Labels
    QString name = newLabel->getLabelName();
    if (name.isEmpty())
        return;

    ui->listWidget->addItem(name);

    // Add Label to vector
    my_Labels.push_back(*newLabel);
}

///////////////////////////////////////

void MainWindow::on_editButton_clicked()
{
    int index = ui -> listWidget-> currentRow();
    if (index<0)
    {
        QMessageBox::warning(this, tr("Edit Label"), tr("Please, select a Label to edit"));
        return;
    }

    LabelDialog editLabel;
    editLabel.setWindowTitle("Edit Current Label");
    editLabel.setModal(true);
    editLabel.setMode(1);
    editLabel.setLabelAttributes(my_Labels[index]);
    editLabel.exec();

    // Get edited Label attributes
    Label *editedLabel = new Label;
    *editedLabel=editLabel.getMyLabel();

    // Avoid error Labels
    QString name = editedLabel->getLabelName();
    if (name.isEmpty())
        return;

    // Modificate item in listWidget
    ui->listWidget->item(index)->setText(editedLabel->getLabelName());

    // Edited Label attributes
    my_Labels[index].setLabelName(editedLabel->getLabelName());
    my_Labels[index].setLabelID(editedLabel->getLabelID());
    my_Labels[index].setLabelColor(editedLabel->getLabelColor());
    my_Labels[index].setLabelInstanciableState(editedLabel->getLabelInstanciableState());
    my_Labels[index].setLabelBoundingBoxState(editedLabel->getLabelBoundingBoxState());

    // If the Edited Label is in the Selected Area, set atributes
    if (index == index_label)
    {
        //Name
        QString name = my_Labels[index_label].getLabelName();
        ui -> line_LabelName -> setText(name);

        //ID
        int id = my_Labels[index_label].getLabelID();
        QString id_string = QString::number(id,10);
        ui -> line_LabelID -> setText(id_string);

        //Color
        QColor color = my_Labels[index_label].getLabelColor();
        QPalette pal = ui -> LabelColor_Widget -> palette();
        pal.setColor(QPalette::Window, color);
        ui -> LabelColor_Widget -> setPalette(pal);

        //Instanciable
        bool instc_state = my_Labels[index_label].getLabelInstanciableState();
        if (instc_state==true)
            ui->lineInstanciable->setText("Yes");
        else if (instc_state==false)
            ui->lineInstanciable->setText("No");

        //Bounding Box
        bool boundingbox_state = my_Labels[index_label].getLabelBoundingBoxState();
        if (boundingbox_state==true)
            ui->lineBBox->setText("Yes");
        else if (boundingbox_state==false)
            ui->lineBBox->setText("No");
    }

    // Update polygones and vertexes on image
    paint();
}

///////////////////////////////////////

void MainWindow::on_deleteButton_clicked()
{
    int index = ui -> listWidget-> currentRow();
    if (index<0)
    {
        QMessageBox::warning(this, tr("Delete Label"), tr("Please, select a Label to delete"));
        return;
    }

    my_Labels.erase(my_Labels.begin()+index);
    delete ui -> listWidget -> currentItem();

    // If the Deleted Label is in the Selected Label Area, clean atributes
    if (index == index_label)
    {
        labelSelected=false;

        //Name
        ui -> line_LabelName -> clear();

        //ID
        ui -> line_LabelID -> clear();

        //Color
        ui -> LabelColor_Widget -> setAutoFillBackground(false);

        //Instanciable
        ui-> lineInstanciable -> clear();

        //Bounding Box
        ui -> lineBBox -> clear();
    }

    // If the Deleted Label has a lower index than index_label, update this variable
    if (index<index_label)
        index_label--;

    // Update polygones and vertexes on image
    paint();
}

///////////////////////////////////////

void MainWindow::on_selectButton_clicked()
{    
    int index = ui -> listWidget-> currentRow();
    if (index<0)
    {
        QMessageBox::warning(this, tr("Select Label"), tr("Please, select a Label"));
        return;
    }
    else{
        if(index_label!=-1 && my_Labels[index_label].getNumVertexes()>0)
        {
            QMessageBox newDecision;
            newDecision.setWindowTitle("Change label");
            newDecision.setText("Do you want to change label?");
            newDecision.setInformativeText("If you change, you'll lose current Vertexes. "
                                           "You can click 'No' and create a polygon, to save Vertexes.");
            newDecision.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            newDecision.setDefaultButton(QMessageBox::No);
            newDecision.setIcon(QMessageBox::Question);
            int ret = newDecision.exec();

            if (ret == QMessageBox::No)
                return;

            else if (ret == QMessageBox::Yes)
            {
                int vertexes = my_Labels[index_label].getNumVertexes();

                // Delete all vertexes
                for (int i=0; i<vertexes; i++)
                    my_Labels[index_label].deleteLastVertex();

                paint();
            }
        }

        index_label=index;
        labelSelected=true;

        //Name
        QString name = my_Labels[index_label].getLabelName();
        ui -> line_LabelName -> setText(name);

        //ID
        int id = my_Labels[index_label].getLabelID();
        QString id_string = QString::number(id,10);
        ui -> line_LabelID -> setText(id_string);

        //Color
        ui -> LabelColor_Widget -> setAutoFillBackground(true);
        QColor color = my_Labels[index_label].getLabelColor();
        QPalette pal = ui -> LabelColor_Widget -> palette();
        pal.setColor(QPalette::Window, color);
        ui -> LabelColor_Widget -> setPalette(pal);

        //Instanciable
        bool instc_state = my_Labels[index_label].getLabelInstanciableState();
        if (instc_state==true)
            ui->lineInstanciable->setText("Yes");
        else if (instc_state==false)
            ui->lineInstanciable->setText("No");

        //Bounding Box
        bool boundingbox_state = my_Labels[index_label].getLabelBoundingBoxState();
        if (boundingbox_state==true)
            ui->lineBBox->setText("Yes");
        else if (boundingbox_state==false)
            ui->lineBBox->setText("No");
    }
}

///////////////////////////////////////

void MainWindow::exportDatasetAction()
{
    // Only export if there are an image or a video opened
    if(auxmenu=='n')
        return;

    DatasetDialog selectDataset;
    selectDataset.setWindowTitle("Select Dataset");
    selectDataset.setModal(true);
    if(auxmenu=='v')
        selectDataset.enableCurrentFrame();
    selectDataset.exec();

    bool selected_state = selectDataset.getSelectedState();

    if(!selected_state)
        return;

    // Dataset options selected
    bool *pDataset = selectDataset.getSelection();

    bool colorOption=*pDataset;
    bool idOption=*(pDataset+1);
    bool instOption=*(pDataset+2);
    bool polygonesOption=*(pDataset+3);
    bool currentFrame = false;
    if(auxmenu=='v')
        currentFrame=*(pDataset+4);

    // Variables
    int num_labels = my_Labels.size();
    Polygon Lbl_polygon;
    int num_Lbl_polygns, num_plg_vertexes;

    // File Name
    string fileNameString = fileName.toStdString();
    int sizeFileName = fileNameString.length();
    fileNameString.erase(sizeFileName-4); // Dirección sin el .jpg, .png, .avi, .mpg

    if (auxmenu == 'v')
    {
        fileNameString = fileNameString + '_';

        // Add current frame to the name of the file
        stringstream stringCurrentFrame;
        stringCurrentFrame << fileNameString << current_frame;

        fileNameString = stringCurrentFrame.str();
    }

    // Color Option selected
    if (colorOption==true)
    {
        Mat colorsImg(height_image, width_image, CV_8UC3, Scalar(0,0,0));

        QColor Lbl_color;

        for (int i=0; i<num_labels; i++)
        {
            num_Lbl_polygns=my_Labels[i].getNumPolygones();
            Lbl_color=my_Labels[i].getLabelColor();

            for (int j=0; j<num_Lbl_polygns; j++)
            {
                Lbl_polygon = my_Labels[i].getPolygon(j);

                num_plg_vertexes = Lbl_polygon.getPolyNumVert();

                Point poly_Vertexes[num_plg_vertexes];
                for (int k=0; k<num_plg_vertexes; k++)
                    poly_Vertexes[k] = Lbl_polygon.getPolyVertex(k);

                fillConvexPoly(colorsImg, poly_Vertexes, num_plg_vertexes,
                               Scalar(Lbl_color.blue(), Lbl_color.green(), Lbl_color.red()));
            }
        }

        // Save image
        string fileNameColor = fileNameString + "_colors.jpg";

        imwrite(fileNameColor, colorsImg);
    }

    // ID (graylevel) Option selected
    if (idOption==true)
    {
        Mat idImg(height_image, width_image, CV_8UC3, Scalar(0,0,0));

        int Lbl_ID;

        for (int i=0; i<num_labels; i++)
        {
            num_Lbl_polygns=my_Labels[i].getNumPolygones();
            Lbl_ID=my_Labels[i].getLabelID();

            for (int j=0; j<num_Lbl_polygns; j++)
            {
                Lbl_polygon = my_Labels[i].getPolygon(j);
                num_plg_vertexes = Lbl_polygon.getPolyNumVert();

                Point poly_Vertexes[num_plg_vertexes];
                for (int k=0; k<num_plg_vertexes; k++)
                    poly_Vertexes[k] = Lbl_polygon.getPolyVertex(k);

                fillConvexPoly(idImg, poly_Vertexes, num_plg_vertexes, Scalar(Lbl_ID, Lbl_ID, Lbl_ID));
            }
        }

        // Save image
        string fileNameID = fileNameString + "_IDs.jpg";

        imwrite(fileNameID, idImg);
    }

    // Instanciable Option selected
    if (instOption==true)
    {
        Mat instImg(height_image, width_image, CV_8UC3, Scalar(0,0,0));

        bool Lbl_inst;
        int Lbl_ID;

        for (int i=0; i<num_labels; i++)
        {
            Lbl_inst=my_Labels[i].getLabelInstanciableState();
            Lbl_ID=my_Labels[i].getLabelID();

            if(Lbl_inst==true)
            {
                num_Lbl_polygns=my_Labels[i].getNumPolygones();

                for (int j=0; j<num_Lbl_polygns; j++)
                {
                    Lbl_polygon = my_Labels[i].getPolygon(j);
                    num_plg_vertexes = Lbl_polygon.getPolyNumVert();

                    Point poly_Vertexes[num_plg_vertexes];
                    for (int k=0; k<num_plg_vertexes; k++)
                        poly_Vertexes[k] = Lbl_polygon.getPolyVertex(k);

                    fillConvexPoly(instImg, poly_Vertexes, num_plg_vertexes, Scalar(Lbl_ID, Lbl_ID, Lbl_ID));
                }
            }
        }

        // Save image
        string fileNameInst = fileNameString + "_instanciables.jpg";

        imwrite(fileNameInst, instImg);
    }

    // Polygones Option selected
    if (polygonesOption==true)
    {
        // Save File Polygones Name
        string fileNamePoly = fileNameString + "_Polygones.xml";
        const char *fileNamePolyChar = fileNamePoly.c_str();

        // Create XML document
        XMLDocument xml_Polygones;

        // Create pointer to Root
        XMLNode *pRoot = xml_Polygones.NewElement("Root");
        xml_Polygones.InsertFirstChild(pRoot);

        // Variables
        QString nameLabel;
        string nameLabelStrg;
        const char *nameLabelChar;
        QColor colorLabel;
        int numLabels, numPolygns, numVertexes, IDLabel;
        bool instLabel, bboxLabel;
        Polygon polygon;
        Point vertex, topleftBBox;
        Rect boundingBox;
        Size sizeBBox;

        // Pointers Element
        XMLElement *pNumLabel, *pLabelElement, *pNameElement, *pIDElement, *pColorElement,
                *pRedElement, *pGreenElement, *pBlueElement, *pInstElement, *pBBoxStateElement,
                *pNumPolygones,*pPolygonElement, *pNumVertex, *pVertexElement, *pXElement, *pYElement,
                *pBBoxElement, *pBBoxXElement, *pBBoxYElement, *pBBoxHeight, *pBBoxWidth;

        // Num of labels
        pNumLabel = xml_Polygones.NewElement("NumLabels");
        numLabels = my_Labels.size();
        pNumLabel->SetText(numLabels);
        pRoot->InsertEndChild(pNumLabel);

        for (int i=0; i<my_Labels.size(); i++)
        {
            // Open Label
            pLabelElement = xml_Polygones.NewElement("Label");

            // Name
            pNameElement = xml_Polygones.NewElement("Name");

            nameLabel=my_Labels[i].getLabelName();
            nameLabelStrg=nameLabel.toStdString();
            nameLabelChar=nameLabelStrg.c_str();

            pNameElement->SetText(nameLabelChar);
            pLabelElement->InsertEndChild(pNameElement);

            // ID
            pIDElement = xml_Polygones.NewElement("ID");
            IDLabel=my_Labels[i].getLabelID();
            pIDElement->SetText(IDLabel);
            pLabelElement->InsertEndChild(pIDElement);

            // Color
            pColorElement = xml_Polygones.NewElement("Color");
            colorLabel=my_Labels[i].getLabelColor();

            pRedElement = xml_Polygones.NewElement("Red");
            pRedElement->SetText(colorLabel.red());
            pColorElement->InsertEndChild(pRedElement);

            pGreenElement = xml_Polygones.NewElement("Green");
            pGreenElement->SetText(colorLabel.green());
            pColorElement->InsertEndChild(pGreenElement);

            pBlueElement = xml_Polygones.NewElement("Blue");
            pBlueElement->SetText(colorLabel.blue());
            pColorElement->InsertEndChild(pBlueElement);

            pLabelElement->InsertEndChild(pColorElement);

            // Instanciable State
            pInstElement = xml_Polygones.NewElement("Instanciable");
            instLabel=my_Labels[i].getLabelInstanciableState();
            pInstElement->SetText(instLabel);
            pLabelElement->InsertEndChild(pInstElement);

            // Bounding Box State
            pBBoxStateElement = xml_Polygones.NewElement("BoundingBox");
            bboxLabel=my_Labels[i].getLabelBoundingBoxState();
            pBBoxStateElement->SetText(bboxLabel);
            pLabelElement->InsertEndChild(pBBoxStateElement);

            // Num of Polygones
            pNumPolygones = xml_Polygones.NewElement("NumPolygones");
            numPolygns = my_Labels[i].getNumPolygones();
            pNumPolygones->SetText(numPolygns);
            pLabelElement->InsertEndChild(pNumPolygones);

            for (int j=0; j<numPolygns; j++)
            {
                polygon = my_Labels[i].getPolygon(j);

                // Open Polygon
                pPolygonElement = xml_Polygones.NewElement("Polygon");

                // Num of Vertexes
                pNumVertex = xml_Polygones.NewElement("NumVertexes");
                numVertexes = polygon.getPolyNumVert();
                pNumVertex->SetText(numVertexes);
                pPolygonElement->InsertEndChild(pNumVertex);

                // Vertexes Position
                for (int k=0; k<numVertexes; k++)
                {
                    vertex = polygon.getPolyVertex(k);

                    // Open Vertex
                    pVertexElement = xml_Polygones.NewElement("Vertex");

                    // X coordinade
                    pXElement = xml_Polygones.NewElement("X");
                    pXElement->SetText(vertex.x);
                    pVertexElement->InsertEndChild(pXElement);

                    // Y coordinade
                    pYElement = xml_Polygones.NewElement("Y");
                    pYElement->SetText(vertex.y);
                    pVertexElement->InsertEndChild(pYElement);

                    // Close Vertex
                    pPolygonElement->InsertEndChild(pVertexElement);
                }

                // Bounding Box
                if (bboxLabel==true)
                {
                    boundingBox = boundingRect(polygon.getPolyVertexes());
                    topleftBBox = boundingBox.tl();
                    sizeBBox = boundingBox.size();

                    // OpenBoudingBox
                    pBBoxElement = xml_Polygones.NewElement("BoundingBoxDimensions");

                    // X coordinade top-left point
                    pBBoxXElement = xml_Polygones.NewElement("XBBox");
                    pBBoxXElement->SetText(topleftBBox.x);
                    pBBoxElement->InsertEndChild(pBBoxXElement);

                    // Y coordinade top-left point
                    pBBoxYElement = xml_Polygones.NewElement("YBBox");
                    pBBoxYElement->SetText(topleftBBox.y);
                    pBBoxElement->InsertEndChild(pBBoxYElement);

                    // Height of bounding box
                    pBBoxHeight = xml_Polygones.NewElement("HeightBBox");
                    pBBoxHeight->SetText(sizeBBox.height);
                    pBBoxElement->InsertEndChild(pBBoxHeight);

                    // Width of bounding box
                    pBBoxWidth = xml_Polygones.NewElement("WidthBBox");
                    pBBoxWidth->SetText(sizeBBox.width);
                    pBBoxElement->InsertEndChild(pBBoxWidth);

                    // Close Bounding Box
                    pPolygonElement->InsertEndChild(pBBoxElement);
                }

                //Close Polygon
                pLabelElement->InsertEndChild(pPolygonElement);
            }

            // Close Label
            pRoot->InsertEndChild(pLabelElement);
        }

        xml_Polygones.SaveFile(fileNamePolyChar, false);
    }

    // Current Frame selected
    if (currentFrame==true)
    {
        // Save frame
        string fileNameFrame = fileNameString + ".jpg";

        imwrite(fileNameFrame, frame);
    }

    // If any option was selected
    if (colorOption || idOption || instOption || polygonesOption || currentFrame)
        QMessageBox::information(this, tr("Dataset exported"), tr("Selected Dataset exported successfully"));

    newPolygnsSaved = true;
}

///////////////////////////////////////

void MainWindow::exportLabelsAction()
{
    QString saveFileName = QFileDialog::getSaveFileName(this, tr("Save File"), QDir::currentPath(), tr("XML files (*.xml)"));

    string saveFileNameString = saveFileName.toStdString();
    saveFileNameString = saveFileNameString +".xml";
    const char *saveFileNameChar = saveFileNameString.c_str();

    // Create XML document
    XMLDocument xml_Labels;

    // Create pointer to Root
    XMLNode *pRoot = xml_Labels.NewElement("Root");
    xml_Labels.InsertFirstChild(pRoot);

    // Variables
    QString nameLabel;
    string nameLabelStrg;
    const char *nameLabelChar;
    QColor colorLabel;
    int numLabels, IDLabel;
    bool instLabel, bboxLabel;

    // Pointers Element
    XMLElement *pNumLabel, *pLabelElement, *pNameElement, *pIDElement, *pColorElement,
            *pRedElement, *pGreenElement, *pBlueElement, *pInstElement, *pBBoxElement;

    // Num of labels
    pNumLabel = xml_Labels.NewElement("NumLabels");
    numLabels = my_Labels.size();
    pNumLabel->SetText(numLabels);
    pRoot->InsertEndChild(pNumLabel);

    for (int i=0; i<numLabels; i++)
    {
        // Open Label
        pLabelElement = xml_Labels.NewElement("Label");

        // Name
        pNameElement = xml_Labels.NewElement("Name");

        nameLabel=my_Labels[i].getLabelName();
        nameLabelStrg=nameLabel.toStdString();
        nameLabelChar=nameLabelStrg.c_str();

        pNameElement->SetText(nameLabelChar);
        pLabelElement->InsertEndChild(pNameElement);

        // ID
        pIDElement = xml_Labels.NewElement("ID");
        IDLabel=my_Labels[i].getLabelID();
        pIDElement->SetText(IDLabel);
        pLabelElement->InsertEndChild(pIDElement);

        // Color
        pColorElement = xml_Labels.NewElement("Color");
        colorLabel=my_Labels[i].getLabelColor();

        pRedElement = xml_Labels.NewElement("Red");
        pRedElement->SetText(colorLabel.red());
        pColorElement->InsertEndChild(pRedElement);

        pGreenElement = xml_Labels.NewElement("Green");
        pGreenElement->SetText(colorLabel.green());
        pColorElement->InsertEndChild(pGreenElement);

        pBlueElement = xml_Labels.NewElement("Blue");
        pBlueElement->SetText(colorLabel.blue());
        pColorElement->InsertEndChild(pBlueElement);

        pLabelElement->InsertEndChild(pColorElement);

        // Instanciable State
        pInstElement = xml_Labels.NewElement("Instanciable");
        instLabel=my_Labels[i].getLabelInstanciableState();
        pInstElement->SetText(instLabel);
        pLabelElement->InsertEndChild(pInstElement);

        // Bounding Box State
        pBBoxElement = xml_Labels.NewElement("BoundingBox");
        bboxLabel=my_Labels[i].getLabelBoundingBoxState();
        pBBoxElement->SetText(bboxLabel);
        pLabelElement->InsertEndChild(pBBoxElement);

        // Close Label
        pRoot->InsertEndChild(pLabelElement);
    }

    xml_Labels.SaveFile(saveFileNameChar, false);
    QMessageBox::information(this, tr("Labels exported"), tr("Labels Set exported successfully"));
}

///////////////////////////////////////
