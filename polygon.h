#ifndef POLYGON
#define POLYGON

#include <QWidget>
#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;


class Polygon
{
    vector <Point> polyVertexes;
    int n_vertexes;
    QColor polyColor;

public:
    Polygon();
    Polygon(vector<Point> vertexes, QColor color);
    Polygon(const Polygon &Plg);
    ~Polygon();

    Point getPolyVertex(int index);
    vector<Point> getPolyVertexes();
    int getPolyNumVert();
    QColor getPolyColor();
};

#endif // POLYGON
