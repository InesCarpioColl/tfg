﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#ifndef QT_NO_PRINTER
#include <QtPrintSupport/QPrinter>
#endif

#include <QtWidgets>
#include <QFileDialog>
#include <QMessageBox>
#include <opencv2/opencv.hpp>
#include <iostream>
#include "tinyxml2.h"
#include "label.h"
#include "labeldialog.h"
#include "datasetdialog.h"

using namespace std;
using namespace cv;
using namespace tinyxml2;

#ifndef QT_NO_PRINTER
#include <QtPrintSupport/QPrintDialog>
#endif

class Label;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void nextVideoFrame();
    void prevVideoFrame();
    void getInterval();

    void nextImage();
    void prevImage();
    void update_currentFrame();


    bool scaledImage();
    void setLimits();
    bool searchPolygonesFile();

    void mouseDoubleClickEvent(QMouseEvent *doubleclick);
    void paint();

private Q_SLOTS:
    void openImage();
    void openVideo();
    void openLabels();
    void exportDatasetAction();
    void exportLabelsAction();

    void on_prevButton_clicked();
    void on_nextButton_clicked();

    void on_labellingButton_toggled(bool checked);
    void on_deleteVertexButton_clicked();
    void on_deletePolyButton_clicked();

    void on_addButton_clicked();
    void on_editButton_clicked();
    void on_deleteButton_clicked();
    void on_selectButton_clicked();


private:

    Ui::MainWindow *ui;
    QLabel *label_image;

    QScrollArea *scrollArea;

    QAction *openImageAct;
    QAction *openVideoAct;
    QAction *openLabelsAct;
    QAction *exportDatasetAct;
    QAction *exportLabelsAct;

    // General variables
    char auxmenu; // Video o image
    QString fileName;

    // Images variables
    QStringList imageFilesNames;
    int image_index; // Current image
    int num_image;
    Mat cvImg; // OpenCV image

    // Video variables
    int interval;
    VideoCapture cap;
    double frame_msec;
    Mat frame;
    int num_frame, current_frame;

    // Limit of image variables
    QPoint P1, P2;
    int height_image, width_image;
    int height_label, width_label;

    // Labels variables
    vector<Label> my_Labels;
    int index_label; // Index of current Label Selected

    // Labelling variables
    bool labelling_checked;
    bool labelSelected;
    Mat imageDrawn;
    bool newPolygnsSaved;
};

#endif // MAINWINDOW_H
